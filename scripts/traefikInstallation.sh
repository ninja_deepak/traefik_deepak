#!/bin/bash 

Action=$1



installUbuntu() {

  sudo apt-get update

  #Installing latest binary from https://github.com/containous/traefik/releases.


  wget https://github.com/traefik/traefik/releases/download/v2.6.0/traefik_v2.6.0_linux_386.tar.gz
  tar -xvzf traefik_v2.6.0_linux_386.tar.gz
  rm -rf traefik_v2.6.0_linux_386.tar.gz.*
  chmod +x traefik


#putting traefik to system wide boundary
systemBinary() {

    if [ -f /usr/local/bin/traefik  ]
      then
        sudo rm -rf /usr/local/bin/traefik
        sudo cp traefik /usr/local/bin/
        sudo chown root:root /usr/local/bin/traefik
        sudo chmod 755 /usr/local/bin/traefik
        sudo setcap 'cap_net_bind_service=+ep' /usr/local/bin/traefik
      else
        sudo cp traefik /usr/local/bin/
        sudo chown root:root /usr/local/bin/traefik
        sudo chmod 755 /usr/local/bin/traefik
        sudo setcap 'cap_net_bind_service=+ep' /usr/local/bin/traefik
    fi
}

#setting up traefik user and group.

userandGroups () {

var=$(cat /etc/passwd | grep -o "traefik")

if [ $var == "traefik" ]
then
  echo "Traefik already exist hence not creating this time, Script will move forward to do other installations."
  else
    sudo groupadd -g 321 traefik
    sudo useradd \
      -g traefik --no-user-group \
      --home-dir /var/www --no-create-home \
      --shell /usr/sbin/nologin \
      --system --uid 321 traefik
fi

}

makeDirectory () {

  if [ -d /etc/traefik ]

  then 
    sudo rm -rf /etc/traefik
    sudo mkdir /etc/traefik
    sudo mkdir /etc/traefik/acme
    sudo mkdir /etc/traefik/dynamic
    sudo chown -R root:root /etc/traefik
    sudo chown -R traefik:traefik /etc/traefik/dynamic

  else

    sudo mkdir /etc/traefik
    sudo mkdir /etc/traefik/acme
    sudo mkdir /etc/traefik/dynamic
    sudo chown -R root:root /etc/traefik
    sudo chown -R traefik:traefik /etc/traefik/dynamic
  fi
}

makeLogfiles () {

  if [ -f /var/log/traefik.log ] 
    then
      sudo rm -rf /var/log/traefik.log 
      sudo touch /var/log/traefik.log
      sudo chown traefik:traefik /var/log/traefik.log
    else
      sudo touch /var/log/traefik.log
      sudo chown traefik:traefik /var/log/traefik.log
  fi

}

makeConfigfiles () {

    sudo cp -r /home/ubuntu/traefik_deepak/configfiles/traefik.toml /etc/traefik/
    sudo cp -r /home/ubuntu/traefik_deepak/configfiles/redirect.toml /etc/traefik/dynamic/

}

makeServicefile () {

  if [ -f /etc/systemd/system/traefik.service ] 
      then
        sudo rm -rf /etc/systemd/system/traefik.service
        sudo cp -r /home/ubuntu/traefik_deepak/configfiles/traefik.service /etc/systemd/system/
        sudo chown root:root /etc/systemd/system/traefik.service
        sudo chmod 644 /etc/systemd/system/traefik.service
        sudo systemctl daemon-reload
        sudo systemctl start traefik.service
      else
        sudo cp -r /home/ubuntu/traefik_deepak/configfiles/traefik.service /etc/systemd/system/
        sudo chown root:root /etc/systemd/system/traefik.service
        sudo chmod 644 /etc/systemd/system/traefik.service
        sudo systemctl daemon-reload
        sudo systemctl start traefik.service
  fi

 }

}

case $Action in

  Ubuntu)

    installUbuntu
    systemBinary
    userandGroups
    makeDirectory
    makeLogfiles
    makeConfigfiles
    makeServicefile

    ;;

  *)
    echo "This is for ubuntu as of now."
    ;;

esac


